<footer class="mt-5 text-center">
    <ul class="mb-5">
        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
        <li><a href=""><i class="fab fa-twitter"></i></a></li>
        <li><a href=""><i class="fab fa-vimeo-v"></i></a></li>
    </ul>
    <div class="row mb-2">
        <div class="col-sm-4 p-0"><a href="">Dashboard</a></div>
        <div class="col-sm-4 p-0"><a href="">Recipe Library</a></div>
        <div class="col-sm-4 p-0"><a href="">Profile</a></div>
    </div>
    <div class="row text-center mb-4">
        <small class=""><i class="fal fa-copyright"></i> 2019 F45 Training Pty Ltd - <a href="" class="font-weight-normal">F45 Training</a></small>
        <small>All Rights Reserved. <a href="" class="font-weight-normal">Privacy Policy</a> <span>|</span> <a href="" class="font-weight-normal">Terms & Conditions</a></small>
    </div>
</footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    <!-- Custom JavaScript -->
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>