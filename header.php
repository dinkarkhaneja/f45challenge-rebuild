<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    
    <title>Starter Template · Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Stylesheets -->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/page.css" rel="stylesheet" type="text/css">
    <link href="css/fonts.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" href="vendors/fontawesome-pro-5.6.1/css/all.css">
</head>

<body id="f45">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top pn-semibold fs-large">
        <div class="container">
            <a class="navbar-brand" href="#"></a>
            
            <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="text-uppercase font-weight-bold menu-content">Menu</span>
                <span class="menu-cross d-none"><img src="img/cross1.png"></span>
            </button>
            <a class="mq-sign d-block d-md-none f45-white f45-pointer">Sign In</a>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pr-4">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Register</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto d-none d-md-block">
                <li class="nav-item pr-3">
                    <a class="nav-link" href="#">Sign In <span class="sr-only">(current)</span></a>
                </li>
            </ul>    
        </div>
        </div>
    </nav>