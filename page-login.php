<?php require("header.php"); ?>
    <main role="main" class="container f45-login">
        <div class="mt-5 row">
            <div class="col-md-8">
                <h2 class="f45-primary mb-4">Login</h2>
                <!-- Login Form -->
                <form>
                    <fieldset class="mb-4">
                        <label for="login-email" class="d-block">Email</label>
                        <input type="email" id="login-email">
                    </fieldset>
                    <fieldset class="mb-4">
                        <label for="login-pass" class="d-block">Password</label>
                        <input type="password" id="login-pass">
                    </fieldset>
                    <fieldset class="mb-4">
                        <label class="cb-container">Remember Me
                          <input class="checkbox" type="checkbox">
                          <span class="checkmark"></span>
                        </label>
                    </fieldset>
                    <fieldset class="mb-3">
                        <img src="img/spinner-custom1.png" class="bg-loader d-none">
                        <input type="submit" value="Log In" class="f45-pointer f45-button-primary login-btn">
                    </fieldset>
                </form>
                <div>
                    <a class="f45-pointer">Register</a>
                    <span>|</span>
                    <a class="f45-pointer">Lost Your Password ?</a>
                </div>
            </div>
            <div class="col-md-4 pl-4">
                <h6 class="font-weight-bold mb-4">Safe and Secure Login</h6>
                <img src="img/lock.jpg" class="mb-3 img-fluid">
                <p>Your details are protected by SSL security.</p>
            </div>
        </div>
    </main><!-- /.container -->
<?php require("footer.php"); ?>